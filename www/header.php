    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
    <script type="text/javascript" src="asset/js/vendor/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="asset/css/style.css">
    <script type="text/javascript" src="asset/js/vendor/custom.modernizr.js"></script>
    <!--<script type="text/javascript" src="asset/js/kinetic-v4.7.4.min.js"></script>-->
  </head>
  <body>
    <header>
      <div class="nav-wrap">
        <div class="font-signika" id="hdr-title"><a href="index.php" class="clean">Steven Enamakel</a></div>
        <nav>
          <ul class="hide-for-medium-down">
            <li><a href="design.php">Design</a><i></i></li>
            <li><a href="programming.php">Programming</a><i></i></li>
            <li><a href="contact.php">Contact</a><i></i></li>
          </ul>
        </nav>
      </div>
    </header>