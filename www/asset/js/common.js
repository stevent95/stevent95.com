/*
 * Javascript for the site goes here. Everything in one file to minimize HTTP requests/
 */

var hdr_ht = 70;
var window_ht = $(window).height();
var recommended_ht = window_ht - hdr_ht;

$(".fullpage-template").each(function(){
  //console.log(recommended_ht);
  if($(this).height() < recommended_ht)
    $(this).height(recommended_ht);
});

//Smooth Scrolling function.
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 600);
        return false;
      }
    }
  });
});

//Google Analytics Function.
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-46608607-1', 'stevent95.com');
ga('send', 'pageview');


// Include ./canvas3d.js for the 3D effect with the mouse on the Design page!
var script = document.createElement('script');
script.src = "/asset/js/canvas3d.js";
document.getElementsByTagName('script')[0].parentNode.appendChild(script);