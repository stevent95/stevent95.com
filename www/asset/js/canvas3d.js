$('.canvas-3D').each(function(){
  var that = $(this);
  var layers = that.find(".layer");
  var data = that.data();

  var max = { x:that.width(), y:that.height()};
  
  var scrn_size = {x:$(window).width(), y:$(window).height()};
  var trnsltn_matrx = {x:max.x/scrn_size.x, y:max.y/scrn_size.y};
  console.log(trnsltn_matrx);

  var currentMousePos = { x:-1, y:-1 };
  //Alway keep listening when the mouse is in the canvas's parent.
  that.parent().mousemove(function(event) {
    currentMousePos.x = event.pageX;
    currentMousePos.y = event.pageY;
    // Call this function whenever the mouse's position had been changed.
    refresh();
  });


  /*
   * Returns the displacement of the mouse in the x-axis.
   * (Axis is centered at document's center)
   */
  function xdisp() {
    var disp = currentMousePos.x - max.x/2;
    return (disp > max.x/2 ? max.x/2 : disp);
  }

  /*
   * Returns the displacement of the mouse in the y-axis.
   * (Axis is centered at document's center)
   */
  function ydisp() {
    var disp = currentMousePos.y - max.y/2;
    return disp;
  }

  /*
   * Code that re-arranges the layers based on the mouse's displacement.
   */
  function refresh(){
    layers.each(function(){
      var fl = $(this).data("focal-length");
      var rev = ($(this).data("reverse") == "" ? -1 : 1);
        
      if(data['vertical'] == "") {
        console.log(ydisp() * trnsltn_matrx.y * rev * fl);
        $(this).css('top',
          ydisp() * trnsltn_matrx.y * rev * fl
        );
      }
      if(data['horizontal'] == "") {
        $(this).css('left',
          max.x/2 + xdisp() * trnsltn_matrx.x * rev * fl - $(this).width()/2
        );
      }
    });
  }

  // Last CSS additions before we are good to go..
  layers.each(function(){
    $(this).css('position', 'absolute');
    $(this).css('left', '40%');
  });
  that.height(data['height']);
});