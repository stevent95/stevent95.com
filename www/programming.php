<!DOCTYPE html>
<html>
  <head>
    <title>Steven Enamakel</title>
<?php  include 'header.php'; ?>
    <!--Mainbody of the page-->
    <section id="main-body">
      <!--Homepage-->
      <section id="page-programming" class="fullpage-template">
        <!--container for the terminal-->
        <section class="terminal">
          <pre>GNU bash, version 4.2.25(1)-release (i686-pc-linux-gnu)</pre>
          <pre>Copyright (C) 2011 Free Software Foundation, Inc.</pre>
          <pre>License GPLv3+: GNU GPL version 3 or later http//gnu.org/licenses/gpl.html</pre>
          <pre> </pre>
          <pre>This is free software; you are free to change and redistribute it.</pre>
          <pre>There is NO WARRANTY, to the extent permitted by law.</pre>
          <pre> </pre>
          <pre>guest@stevent95.com:~$ cat programming_experience.txt</pre>
          <pre> </pre>
          <pre>Software languages:</pre>
          <pre> </pre>
          <pre>                        ___             </pre>
          <pre> 10 |                  |///|            </pre>
          <pre> 9  |   ___            |///|            </pre>
          <pre> 8  |  |///|           |///|            </pre>
          <pre> 7  |  |///|           |///|     ___    </pre>
          <pre> 6  |  |///|           |///|    |///|   </pre>
          <pre> 5  |  |///|           |///|    |///|   </pre>
          <pre> 4  |  |///|    ___    |///|    |///|   </pre>
          <pre> 3  |  |///|   |///|   |///|    |///|   </pre>
          <pre> 2  |  |///|   |///|   |///|    |///|   </pre>
          <pre> 1  |__|///|___|///|___|///|____|///|___</pre>
          <pre>   /    C++     ASM      Py      Java   </pre>
          <pre> </pre>
          <pre>Web-based languages:</pre>
          <pre> </pre>
          <pre> 10 |           ___                     </pre>
          <pre> 9  |   ___    |///|                    </pre>
          <pre> 8  |  |///|   |///|    ___             </pre>
          <pre> 7  |  |///|   |///|   |///|     ___    </pre>
          <pre> 6  |  |///|   |///|   |///|    |///|   </pre>
          <pre> 5  |  |///|   |///|   |///|    |///|   </pre>
          <pre> 4  |  |///|   |///|   |///|    |///|   </pre>
          <pre> 3  |  |///|   |///|   |///|    |///|   </pre>
          <pre> 2  |  |///|   |///|   |///|    |///|   </pre>
          <pre> 1  |__|///|___|///|___|///|____|///|___</pre>
          <pre>   /    HTML    CSS      JS      PHP    </pre>
          <pre> </pre>
          <pre>Web based libraries:</pre>
          <pre>  * Foundation Framework</pre>
          <pre>  * SASS</pre>
          <pre>  * Wordpress</pre>
          <pre>  * CodeIgniter</pre>
          <pre>  * jQuery</pre>
          <pre>  * AngularJS</pre>
          <pre> </pre>
          <pre>guest@stevent95.com:~$ exit</pre>
          <pre>exit</pre>
        </section>
        <!--END container for the terminal-->
    </section>
    <!--END Mainbody-->
<?php  include 'footer.php'; ?>