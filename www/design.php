<!DOCTYPE html>
<html>
  <head>
    <title>Steven Enamakel - Design</title>
<?php include 'header.php'; ?>
    <!--Design-->
    <section id="main-body">
      <section id="page-design" class="fullpage-template">
        <p>Design Page - Coming Soon</p>
        <style>
          .l1{
              width: 10px;height: 500px;
              background: red;
          }
          .l2{
              width: 750px;height: 50px;
              background: green;
          }
          .l3{
              width: 500px; height: 50px;
              background: yellow;
          }
        </style>
        <div class="canvas-3D" data-horizontal data-height=400>
          <div class="layer l1" data-focal-length=0.9 style="z-index: 1;">
            
          </div>
          <div class="layer l2" data-focal-length=0.4 style="z-index: 2;"></div>
          <div class="layer" data-focal-length=0.2 style="z-index: 3; width: 722px; height:226px;">
            <img src="/asset/img/adobe-suite-logo.png" />
          </div>
        </div>
      </section>
    </section>
    <!--END Design-->
<?php include 'footer.php'; ?>