<!DOCTYPE html>
<html>
  <head>
    <title>Steven Enamakel</title>
<?php  include 'header.php'; ?>
    <!--Mainbody of the page-->
    <section id="main-body">
      <!--Homepage-->
      <section id="page-home" class="fullpage-template">
        <h2 class="font-title padding text-center">
          <!--Here goes the home page. Navigation to the other sections go here.<br/>-->
          <!--Arrow keys to navigate (feature comming soom)-->
        </h2>
        <div class="text-cente canvas-3D" data-horizontal data-height=500>
          <section id="diamonds-nav" class="layer" data-focal-length=0.1 data-reverse>
            <a href="design.php">
              <div id="diamond-design" class="diamond-container">
                <div class="shape-diamond"></div>
                <div class="text font-title">Design</div>
              </div>
            </a>
            <a href="contact.php">
              <div id="diamond-more" class="diamond-container">
                <div class="shape-diamond"></div>
                <div class="text font-title">More!</div>
              </div>
            </a>
            <a href="programming.php">
              <div id="diamond-programming" class="diamond-container">
                <div class="shape-diamond"></div>
                <div class="text font-title">Programming</div>
              </div>
            </a>
          </section>
        </div>
      </section>
      <!--END Homepage-->
    </section>
    <!--END Mainbody-->
<?php  include 'footer.php'; ?>